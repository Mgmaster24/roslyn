﻿namespace ChyronHego.Prime.Roslyn.Expressions
{
    using System.Collections.Generic;
    using ChyronHego.Prime.Roslyn.Tokens;
    using Microsoft.CodeAnalysis.VisualBasic.Syntax;

    public class BaseParsingStrategy : IParsingStrategy
    {
        public ExpressionValue Parse(ExpressionSyntax syntax)
        {
            var tokens = new List<IToken>();

            foreach (var token in syntax.DescendantTokens())
            {
                string text = token.Text;
                if (text == ".")
                {
                    continue;
                }

                tokens.Add(new Token { Name = text });
            }

            return new ExpressionValue { Tokens = tokens };
        }
    }
}
