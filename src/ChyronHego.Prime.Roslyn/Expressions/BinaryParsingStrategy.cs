﻿namespace ChyronHego.Prime.Roslyn.Expressions
{
    using System.Collections.Generic;
    using ChyronHego.Prime.Roslyn.Tokens;
    using Microsoft.CodeAnalysis.VisualBasic.Syntax;

    public class BinaryParsingStrategy : IParsingStrategy
    {
        public ExpressionValue Parse(ExpressionSyntax syntax)
        {
            var binaryExpressionSyntax = syntax.TryCast<BinaryExpressionSyntax>();
            var tokens = new List<IToken>();
            tokens.AddRange(ExpressionParser.Instance.ParseExpressionSyntax(binaryExpressionSyntax.Right));
            tokens.Add(new Token { Name = binaryExpressionSyntax.OperatorToken.ValueText });
            tokens.AddRange(ExpressionParser.Instance.ParseExpressionSyntax(binaryExpressionSyntax.Left));

            return new ExpressionValue { Tokens = tokens };
        }
    }
}
