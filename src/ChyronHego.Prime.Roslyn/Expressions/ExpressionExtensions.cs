﻿namespace ChyronHego.Prime.Roslyn.Expressions
{
    using System;

    public static class ExpressionExtensions
    {
        public static T TryCast<T>(this object syntax)
        {
            if (syntax is T t)
            {
                return t;
            }

            throw new InvalidCastException(string.Format(
                "Incorrect strategy was created.  Attempting to cast {0} to type {1}",
                syntax.GetType(),
                typeof(T)));
        }
    }
}
