﻿namespace ChyronHego.Prime.Roslyn.Expressions
{
    using System.Collections.Generic;
    using ChyronHego.Prime.Roslyn.Tokens;
    using Microsoft.CodeAnalysis.VisualBasic;
    using Microsoft.CodeAnalysis.VisualBasic.Syntax;

    public sealed class ExpressionParser
    {
        private static ExpressionParser instance;

        private ExpressionParser()
        {
        }

        public static ExpressionParser Instance => instance ?? (instance = new ExpressionParser());

        public List<IToken> Parse(string codeString)
        {
            return ParseExpressionSyntax(SyntaxFactory.ParseExpression(codeString));
        }

        public List<IToken> ParseExpressionSyntax(ExpressionSyntax expressionSyntax)
        {
            var tokens = new List<IToken>();

            while (true)
            {
                var parsingStrategy = ParsingStrategyFactory.Instance.GetParsingStrategy(expressionSyntax);
                var expressionValue = parsingStrategy.Parse(expressionSyntax);

                foreach (var expressionToken in expressionValue.Tokens)
                {
                    tokens.Add(expressionToken);
                }

                if (expressionValue.Expression == null)
                {
                    break;
                }

                expressionSyntax = expressionValue.Expression;
            }

            return tokens;
        }
    }
}
