﻿namespace ChyronHego.Prime.Roslyn.Expressions
{
    using System.Collections.Generic;
    using ChyronHego.Prime.Roslyn.Tokens;
    using Microsoft.CodeAnalysis.VisualBasic.Syntax;

    public class ExpressionValue
    {
        public List<IToken> Tokens { get; set; }

        public ExpressionSyntax Expression { get; set; }
    }
}
