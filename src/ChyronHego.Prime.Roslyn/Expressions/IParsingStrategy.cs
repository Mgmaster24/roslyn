﻿namespace ChyronHego.Prime.Roslyn.Expressions
{
    using Microsoft.CodeAnalysis.VisualBasic.Syntax;

    public interface IParsingStrategy
    {
        ExpressionValue Parse(ExpressionSyntax syntax);
    }
}
