﻿namespace ChyronHego.Prime.Roslyn.Expressions
{
    using System.Collections.Generic;
    using ChyronHego.Prime.Roslyn.Tokens;
    using Microsoft.CodeAnalysis.VisualBasic.Syntax;

    public class IdentifierParsingStrategy : IParsingStrategy
    {
        public ExpressionValue Parse(ExpressionSyntax syntax)
        {
            var identifierParsingSyntax = syntax.TryCast<IdentifierNameSyntax>();

            return new ExpressionValue
            {
                Tokens = new List<IToken>
                {
                    new Token
                    {
                        Name = identifierParsingSyntax.Identifier.ValueText
                    }
                }
            };
        }
    }
}
