﻿namespace ChyronHego.Prime.Roslyn.Expressions
{
    using System.Collections.Generic;
    using ChyronHego.Prime.Roslyn.Tokens;
    using Microsoft.CodeAnalysis.VisualBasic.Syntax;

    public class InvocationParsingStrategy : IParsingStrategy
    {
        public ExpressionValue Parse(ExpressionSyntax syntax)
        {
            var invocationExpressionSyntax = syntax.TryCast<InvocationExpressionSyntax>();
            var tokens = new List<IToken>();
            int argumentPostion = 0;

            var parentTokens = ExpressionParser.Instance.ParseExpressionSyntax(invocationExpressionSyntax.Expression);
            var methodName = parentTokens[0].Name;

            foreach (var argument in invocationExpressionSyntax.ArgumentList.Arguments)
            {
                var tokenType = argument.GetFirstToken().GetType();
                var argumentToken = new ArgumentToken
                {
                    MethodName = methodName,
                    Name = argument.GetText().ToString(),
                    Position = argumentPostion++
                };

                if (tokenType != null)
                {
                    argumentToken.ArgumentType = tokenType;
                }

                tokens.Add(argumentToken);
            }

            if (tokens.Count == 0)
            {
                tokens.Add(new ArgumentToken
                {
                    MethodName = methodName,
                    Name = "",
                    Position = 0
                });
            }

            return new ExpressionValue { Expression = invocationExpressionSyntax.Expression, Tokens = tokens };
        }
    }
}
