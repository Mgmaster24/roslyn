﻿namespace ChyronHego.Prime.Roslyn.Expressions
{
    using System.Collections.Generic;
    using ChyronHego.Prime.Roslyn.Tokens;
    using Microsoft.CodeAnalysis.VisualBasic.Syntax;

    public class MemberAccessStrategy : IParsingStrategy
    {
        public ExpressionValue Parse(ExpressionSyntax syntax)
        {
            var memberAccessSyntax = syntax.TryCast<MemberAccessExpressionSyntax>();
            var operatorToken = memberAccessSyntax.OperatorToken;
            var memberTokenType = MemberTokenType.Function;
            if (!(memberAccessSyntax.Parent is InvocationExpressionSyntax))
            {
                memberTokenType = MemberTokenType.Property;
            }

            var parentTokens = ExpressionParser.Instance.ParseExpressionSyntax(memberAccessSyntax.Expression);
            string parentName = parentTokens[0].Name;
            var memberToken = new MemberToken
            {
                MemberTokenType = memberTokenType,
                Name = memberAccessSyntax.Name.Identifier.ValueText,
                ParentName = parentName
            };

            return new ExpressionValue { Tokens = new List<IToken> { memberToken }, Expression = memberAccessSyntax.Expression };
        }
    }
}
