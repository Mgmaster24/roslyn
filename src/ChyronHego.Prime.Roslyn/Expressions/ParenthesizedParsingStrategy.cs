﻿namespace ChyronHego.Prime.Roslyn.Expressions
{
    using System.Collections.Generic;
    using ChyronHego.Prime.Roslyn.Tokens;
    using Microsoft.CodeAnalysis.VisualBasic.Syntax;

    public class ParenthesizedParsingStrategy : IParsingStrategy
    {
        public ExpressionValue Parse(ExpressionSyntax syntax)
        {
            var parenthesisSyntax = syntax.TryCast<ParenthesizedExpressionSyntax>();
            var tokens = new List<IToken>
            {
                new Token { Name = parenthesisSyntax.CloseParenToken.ValueText }
            };

            tokens.AddRange(ExpressionParser.Instance.ParseExpressionSyntax(parenthesisSyntax.Expression));

            tokens.Add(new Token { Name = parenthesisSyntax.OpenParenToken.ValueText });

            return new ExpressionValue
            {
                Tokens = tokens
            };
        }
    }
}
