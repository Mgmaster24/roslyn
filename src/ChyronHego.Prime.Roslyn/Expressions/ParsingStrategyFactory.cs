﻿namespace ChyronHego.Prime.Roslyn.Expressions
{
    using System;
    using System.Collections.Generic;
    using Microsoft.CodeAnalysis.VisualBasic.Syntax;

    public sealed class ParsingStrategyFactory
    {
        private static ParsingStrategyFactory instance;

        private readonly Dictionary<Type, IParsingStrategy> strategyDictionary;

        private ParsingStrategyFactory()
        {
            strategyDictionary = new Dictionary<Type, IParsingStrategy>
            {
                { typeof(ExpressionSyntax), new BaseParsingStrategy() },
                { typeof(BinaryExpressionSyntax), new BinaryParsingStrategy() },
                //{ typeof(ElementAccessExpressionSyntax), new ElementAccessStrategy() },
                { typeof(IdentifierNameSyntax), new IdentifierParsingStrategy() },
                { typeof(InvocationExpressionSyntax), new InvocationParsingStrategy() },
                { typeof(MemberAccessExpressionSyntax), new MemberAccessStrategy() },
                { typeof(ParenthesizedExpressionSyntax), new ParenthesizedParsingStrategy() },
                { typeof(UnaryExpressionSyntax), new UnaryParsingStrategy() }
            };
        }

        public static ParsingStrategyFactory Instance => instance ?? (instance = new ParsingStrategyFactory());

        public IParsingStrategy GetParsingStrategy(ExpressionSyntax expressionSyntax)
        {
            IParsingStrategy strategy;
            if (strategyDictionary.TryGetValue(expressionSyntax.GetType(), out strategy))
            {
                return strategy;
            }

            return strategyDictionary[typeof(ExpressionSyntax)];
        }

        public void AddParsingStrategy(Type expressionSyntaxType, Type parsingStrategyType)
        {
            try
            {
                var parsingStrategy = (IParsingStrategy) Activator.CreateInstance(parsingStrategyType);
                strategyDictionary.Add(expressionSyntaxType, parsingStrategy);
            }
            catch (Exception)
            {
                throw new InvalidCastException(string.Format("Unable to create parsing strategy for type {0} provided.", parsingStrategyType));
            }
        }
    }
}
