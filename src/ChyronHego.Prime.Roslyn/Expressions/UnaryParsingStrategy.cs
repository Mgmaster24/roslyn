﻿namespace ChyronHego.Prime.Roslyn.Expressions
{
    using ChyronHego.Prime.Roslyn.Tokens;
    using Microsoft.CodeAnalysis.VisualBasic.Syntax;

    public class UnaryParsingStrategy : IParsingStrategy
    {
        public ExpressionValue Parse(ExpressionSyntax syntax)
        {
            var unarySyntax = syntax.TryCast<UnaryExpressionSyntax>();
            var tokens = ExpressionParser.Instance.ParseExpressionSyntax(unarySyntax.Operand);
            tokens.Add(new Token { Name = unarySyntax.OperatorToken.ValueText });

            return new ExpressionValue
            {
                Tokens = tokens
            };
        }
    }
}
