﻿namespace ChyronHego.Prime.Roslyn.Tokens
{
    using System;

    public class ArgumentToken : Token
    {
        public string MethodName { get; set; }

        public int Position { get; set; }

        public Type ArgumentType { get; set; }
    }
}
