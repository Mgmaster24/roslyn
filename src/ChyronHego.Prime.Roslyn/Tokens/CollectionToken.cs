﻿namespace ChyronHego.Prime.Roslyn.Tokens
{
    public class CollectionToken : Token
    {
        public int Index { get; set; }

        public override string ToString()
        {
            return Name + "[" + Index + "]";
        }
    }
}
