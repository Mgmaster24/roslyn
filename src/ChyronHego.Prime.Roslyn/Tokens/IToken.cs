﻿namespace ChyronHego.Prime.Roslyn.Tokens
{
    public interface IToken
    {
        string Name { get; set; }
    }
}
