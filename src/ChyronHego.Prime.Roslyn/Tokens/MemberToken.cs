﻿namespace ChyronHego.Prime.Roslyn.Tokens
{
    public class MemberToken : Token
    {
        public MemberTokenType MemberTokenType { get; set; }

        public string ParentName { get; set; }

        public override string ToString()
        {
            return "." + Name;
        }
    }
}
