﻿namespace ChyronHego.Prime.Roslyn.Tokens
{
    public enum MemberTokenType
    {
        Function,
        Property
    }
}
