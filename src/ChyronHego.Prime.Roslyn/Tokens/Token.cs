﻿namespace ChyronHego.Prime.Roslyn.Tokens
{
    public class Token : IToken
    {
        public string Name { get; set; }

        public override string ToString()
        {
            return Name;
        }
    }
}
