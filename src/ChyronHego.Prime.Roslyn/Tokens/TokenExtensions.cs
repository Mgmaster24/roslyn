﻿namespace ChyronHego.Prime.Roslyn.Tokens
{
    using System.Collections.Generic;
    using System.Linq;

    public static class TokenExtensions
    {
        public static string ToExpressionString(this List<IToken> tokens)
        {
            string resultText = "";

            for (int i = tokens.Count - 1; i >= 0; i--)
            {
                var token = tokens[i];
                if (!(token is ArgumentToken argumentToken))
                {
                    resultText += token.ToString();

                    var argumentTokens = tokens
                        .OfType<ArgumentToken>()
                        .Where(at => at.MethodName == token.Name)
                        .OrderBy(at => at.Position).ToList();

                    if(argumentTokens.Count > 0)
                    {
                        var argNames = argumentTokens.Select(a => a.Name).ToList();
                        resultText += "(" + string.Join(",", argNames) + ")";
                    }
                }
            }

            return resultText;
        }
    }
}
