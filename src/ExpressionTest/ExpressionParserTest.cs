﻿namespace ExpressionTests
{
    using System.Collections.Generic;
    using System.Text.RegularExpressions;
    using ChyronHego.Prime.Roslyn.Expressions;
    using ChyronHego.Prime.Roslyn.Tokens;
    using Microsoft.VisualStudio.TestTools.UnitTesting;

    [TestClass]
    public class ExpressionParserTest
    {
        [TestMethod]
        public void BinaryExpressionSyntaxTest()
        {
            RunTest("Scene1.Text1.Text = Scene1.Text2.Text");
            RunTest("A + B");
            RunTest("TRUE AND FALSE");
            RunTest("true or false");
        }

        [TestMethod]
        public void InvocationExpressionSyntaxTest()
        {
            RunTest("cos()");
            RunTest("Channels.Channel1.PlayScene(\"Scene1\")");
            RunTest("Channels(0).PlayScene(\"Scene1\")");
            RunTest("Channels.ClearAll()");
            RunTest("Scene1.GetThumbnail(\"a\", 24)");
        }

        [TestMethod]
        public void MemberAccessExpressionSyntaxTest()
        {
            RunTest("Application.Project1.Name");
            RunTest("Channels.Channel1.ActiveScene.Loaded");
            RunTest("Clips(0).Playing");
            RunTest("Scene1.Text1.Color");
            RunTest("Scene1.Expressions(0).Value");
            RunTest("Text1.Text");
        }

        //[TestMethod]
        //public void BinaryExpressionSyntaxTest()
        //{
        //}

        private void RunTest(string testExpression)
        {
            var expressionParser = ExpressionParser.Instance;
            var tokens = expressionParser.Parse(testExpression);
            ExpressionStringsAreEqual(tokens, testExpression);
        }

        private static void ExpressionStringsAreEqual(List<IToken> tokens, string testString)
        {
            Assert.AreEqual(Regex.Replace(testString, @"\s+", ""), tokens.ToExpressionString());
        }
    }
}
