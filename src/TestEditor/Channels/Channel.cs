﻿namespace ChyronHego.Prime.Roslyn.TestEditor.Channels
{
    using ChyronHego.Prime.Roslyn.TestEditor.Commands;

    public class Channel : ICommandObject
    {
        private bool sceneLoaded;
        private bool scenePlaying;

        public int Index { get; set; }

        public string Name { get; set; }

        public string ActiveScene { get; private set; }

        public string Status
        {
            get
            {
                if(string.IsNullOrEmpty(ActiveScene))
                {
                    return "No Active scene";
                }

                string status = "";

                if(sceneLoaded)
                {
                    status += string.Format("Scene {0} is loaded.", ActiveScene);
                }

                if(scenePlaying)
                {
                    if(!string.IsNullOrEmpty(status))
                    {
                        status += "\n";
                    }

                    status += string.Format("Scene {0} is playing.", ActiveScene);
                }

                return status;
            }
        }

        public void OpenScene(string sceneName)
        {
            ActiveScene = sceneName;
        }

        public void LoadScene(string sceneName)
        {
            if(string.IsNullOrEmpty(ActiveScene))
            {
                OpenScene(sceneName);
            }

            sceneLoaded = true;
        }

        public void PlayScene(string sceneName)
        {
            if(!sceneLoaded)
            {
                LoadScene(sceneName);
            }

            scenePlaying = true;
        }

        public void StopScene(string sceneName)
        {
            if (sceneName == ActiveScene)
            {
                scenePlaying = false;
            }
        }

        public void CloseScene(string sceneName)
        {
            if(scenePlaying)
            {
                return;
            }

            if (sceneName == ActiveScene)
            {
                ActiveScene = null;
                scenePlaying = false;
                sceneLoaded = false;
            }
        }
    }
}
