﻿namespace ChyronHego.Prime.Roslyn.TestEditor.Channels
{
    using System.Collections.Generic;

    public class ChannelManager
    {
        public ChannelManager()
        {
            Channels = new List<Channel>();

            Add(new Channel
            {
                Name = "Channel1",
                Index = 0
            });

            Add(new Channel
            {
                Name = "Channel2",
                Index = 1
            });

            Add(new Channel
            {
                Name = "Channel3",
                Index = 2
            });
        }

        public List<Channel> Channels { get; }

        public void Add(Channel channel)
        {
            Channels.Add(channel);
        }

        //public Channel Find(ICommand)

        public object this[int i]
        {
            get { return Channels[i]; }
        }
    }
}
