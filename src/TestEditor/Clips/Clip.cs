﻿namespace ChyronHego.Prime.Roslyn.TestEditor.Clips
{
    using ChyronHego.Prime.Roslyn.TestEditor.Commands;

    public class Clip : ICommandObject
    {
        public string Name { get; set; }

        public int Index { get; set; }

        public bool Play(string name)
        {
            return false;
        }

        public bool Stop(string name)
        {
            return false;
        }
    }
}
