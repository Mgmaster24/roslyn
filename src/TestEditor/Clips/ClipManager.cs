﻿namespace ChyronHego.Prime.Roslyn.TestEditor.Clips
{
    using System.Collections.Generic;

    public class ClipManager
    {
        public ClipManager()
        {
            Clips = new List<Clip>();

            Add(new Clip
            {
                Name = "Channel1",
                Index = 0
            });

            Add(new Clip
            {
                Name = "Channel2",
                Index = 1
            });

            Add(new Clip
            {
                Name = "Channel3",
                Index = 2
            });
        }

        public List<Clip> Clips { get; }

        public void Add(Clip clip)
        {
            Clips.Add(clip);
        }

        public object this[int i]
        {
            get { return Clips[i]; }
        }
    }
}
