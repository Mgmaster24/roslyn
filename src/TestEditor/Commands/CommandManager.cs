﻿namespace ChyronHego.Prime.Roslyn.TestEditor.Commands
{
    using System.Collections.Generic;
    using ChyronHego.Prime.Roslyn.Expressions;
    using ChyronHego.Prime.Roslyn.TestEditor.Scope;
    using ChyronHego.Prime.Roslyn.Tokens;

    public class CommandManager
    {
        private readonly ScopeController scopeController;

        public CommandManager()
        {
            scopeController = new ScopeController();
        }

        public List<IToken> Tokens { get; set; }

        public bool ExecuteCommand(string command)
        {
            Tokens = ExpressionParser.Instance.Parse(command);

            return true;
        }
    }
}
