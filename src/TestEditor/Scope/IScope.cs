﻿namespace ChyronHego.Prime.Roslyn.TestEditor.Scope
{
    using ChyronHego.Prime.Roslyn.TestEditor.Commands;

    public interface IScope
    {
        bool Exists(ICommandObject commandObject);
    }
}
