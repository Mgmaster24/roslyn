﻿namespace ChyronHego.Prime.Roslyn.TestEditor.Scope
{
    using System.Collections.Generic;

    public class ScopeController
    {
        public ScopeController()
        {
            Scopes = new List<IScope>();
        }

        public List<IScope> Scopes { get; }
    }
}
