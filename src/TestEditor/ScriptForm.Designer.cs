﻿namespace ChyronHego.Prime.Roslyn.TestEditor
{
    partial class ScriptForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.applicationMenuStrip = new System.Windows.Forms.MenuStrip();
            this.fileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.newToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.exitToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.codeTextBox = new System.Windows.Forms.TextBox();
            this.resultsTextBox = new System.Windows.Forms.TextBox();
            this.codeLabel = new System.Windows.Forms.Label();
            this.resultsLabel = new System.Windows.Forms.Label();
            this.isScriptCheckBox = new System.Windows.Forms.CheckBox();
            this.evalButton = new System.Windows.Forms.Button();
            this.applicationMenuStrip.SuspendLayout();
            this.SuspendLayout();
            // 
            // applicationMenuStrip
            // 
            this.applicationMenuStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileToolStripMenuItem});
            this.applicationMenuStrip.Location = new System.Drawing.Point(0, 0);
            this.applicationMenuStrip.Name = "applicationMenuStrip";
            this.applicationMenuStrip.Size = new System.Drawing.Size(913, 24);
            this.applicationMenuStrip.TabIndex = 0;
            this.applicationMenuStrip.Text = "menuStrip1";
            // 
            // fileToolStripMenuItem
            // 
            this.fileToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.newToolStripMenuItem,
            this.exitToolStripMenuItem});
            this.fileToolStripMenuItem.Name = "fileToolStripMenuItem";
            this.fileToolStripMenuItem.Size = new System.Drawing.Size(37, 20);
            this.fileToolStripMenuItem.Text = "File";
            // 
            // newToolStripMenuItem
            // 
            this.newToolStripMenuItem.Name = "newToolStripMenuItem";
            this.newToolStripMenuItem.Size = new System.Drawing.Size(98, 22);
            this.newToolStripMenuItem.Text = "New";
            // 
            // exitToolStripMenuItem
            // 
            this.exitToolStripMenuItem.Name = "exitToolStripMenuItem";
            this.exitToolStripMenuItem.Size = new System.Drawing.Size(98, 22);
            this.exitToolStripMenuItem.Text = "Exit";
            this.exitToolStripMenuItem.Click += new System.EventHandler(this.exitToolStripMenuItem_Click);
            // 
            // codeTextBox
            // 
            this.codeTextBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.codeTextBox.Location = new System.Drawing.Point(61, 56);
            this.codeTextBox.Multiline = true;
            this.codeTextBox.Name = "codeTextBox";
            this.codeTextBox.Size = new System.Drawing.Size(721, 120);
            this.codeTextBox.TabIndex = 1;
            // 
            // resultsTextBox
            // 
            this.resultsTextBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.resultsTextBox.Location = new System.Drawing.Point(61, 188);
            this.resultsTextBox.Multiline = true;
            this.resultsTextBox.Name = "resultsTextBox";
            this.resultsTextBox.Size = new System.Drawing.Size(721, 241);
            this.resultsTextBox.TabIndex = 2;
            // 
            // codeLabel
            // 
            this.codeLabel.AutoSize = true;
            this.codeLabel.Location = new System.Drawing.Point(13, 56);
            this.codeLabel.Name = "codeLabel";
            this.codeLabel.Size = new System.Drawing.Size(32, 13);
            this.codeLabel.TabIndex = 3;
            this.codeLabel.Text = "Code";
            // 
            // resultsLabel
            // 
            this.resultsLabel.AutoSize = true;
            this.resultsLabel.Location = new System.Drawing.Point(13, 191);
            this.resultsLabel.Name = "resultsLabel";
            this.resultsLabel.Size = new System.Drawing.Size(42, 13);
            this.resultsLabel.TabIndex = 4;
            this.resultsLabel.Text = "Results";
            // 
            // isScriptCheckBox
            // 
            this.isScriptCheckBox.AutoSize = true;
            this.isScriptCheckBox.Location = new System.Drawing.Point(789, 56);
            this.isScriptCheckBox.Name = "isScriptCheckBox";
            this.isScriptCheckBox.Size = new System.Drawing.Size(64, 17);
            this.isScriptCheckBox.TabIndex = 5;
            this.isScriptCheckBox.Text = "Is Script";
            this.isScriptCheckBox.UseVisualStyleBackColor = true;
            this.isScriptCheckBox.CheckedChanged += new System.EventHandler(this.isScriptCheckBox_CheckedChanged);
            // 
            // evalButton
            // 
            this.evalButton.Location = new System.Drawing.Point(789, 80);
            this.evalButton.Name = "evalButton";
            this.evalButton.Size = new System.Drawing.Size(112, 23);
            this.evalButton.TabIndex = 6;
            this.evalButton.Text = "Evaluate";
            this.evalButton.UseVisualStyleBackColor = true;
            this.evalButton.Click += new System.EventHandler(this.evalButton_Click);
            // 
            // ScriptForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(913, 441);
            this.Controls.Add(this.evalButton);
            this.Controls.Add(this.isScriptCheckBox);
            this.Controls.Add(this.resultsLabel);
            this.Controls.Add(this.codeLabel);
            this.Controls.Add(this.resultsTextBox);
            this.Controls.Add(this.codeTextBox);
            this.Controls.Add(this.applicationMenuStrip);
            this.MainMenuStrip = this.applicationMenuStrip;
            this.Name = "ScriptForm";
            this.Text = "ScriptForm";
            this.applicationMenuStrip.ResumeLayout(false);
            this.applicationMenuStrip.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip applicationMenuStrip;
        private System.Windows.Forms.ToolStripMenuItem fileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem newToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem exitToolStripMenuItem;
        private System.Windows.Forms.TextBox codeTextBox;
        private System.Windows.Forms.TextBox resultsTextBox;
        private System.Windows.Forms.Label codeLabel;
        private System.Windows.Forms.Label resultsLabel;
        private System.Windows.Forms.CheckBox isScriptCheckBox;
        private System.Windows.Forms.Button evalButton;
    }
}