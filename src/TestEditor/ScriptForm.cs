﻿namespace ChyronHego.Prime.Roslyn.TestEditor
{
    using System;
    using System.Windows.Forms;
    using ChyronHego.Prime.Roslyn.TestEditor.Commands;
    using ChyronHego.Prime.Roslyn.Tokens;

    public partial class ScriptForm : Form
    {
        private readonly CommandManager commandManager;

        public ScriptForm()
        {
            InitializeComponent();

            commandManager = new CommandManager();
        }

        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void evalButton_Click(object sender, EventArgs e)
        {
            if (isScriptCheckBox.Checked)
            {
                // Execute or Compile Script
            }
            else
            {
                resultsTextBox.Text = "";
                bool success = commandManager.ExecuteCommand(codeTextBox.Text);
                if(success)
                {
                    resultsTextBox.Text = commandManager.Tokens.ToExpressionString();
                }
            }
        }

        private void isScriptCheckBox_CheckedChanged(object sender, EventArgs e)
        {
            if(isScriptCheckBox.Checked)
            {
                evalButton.Text = "Compile";
            }
            else
            {
                evalButton.Text = "Evaluate";
            }
        }
    }
}
